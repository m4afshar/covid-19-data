# COVID-19 Data

**COVID-19 Data Downloader**

This function returns the latest available data subset of COVID-19. Please bear in mind that you should run it on your personal Desktop with an internet connection.

To use the function, you should simply enter an optional argument of the data subset you are looking for. This subset can be any of the 5 following: 'CONFIRMED', 'DEATH', 'RECOVERED', 'CONFIRMED_US', and 'DEATH_US'. Where first three subsets are the coronavirus related cases globally and the last two ones are the US cases, respectively. If none or a wrong value is chosen as the subset name, the globally confirmed cases is returned. The function will return a pandas DataFrame as an output. The returned dataset is the [COVID-19](https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data) dataset/subset from John Hopkins university.

**Requirments**

- requests
- pandas
- io

Stay Safe!